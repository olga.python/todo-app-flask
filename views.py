from flask import flash, redirect, render_template, request, Blueprint, abort
from werkzeug.security import generate_password_hash, check_password_hash
from models import db, User, Task
from forms import RegisterForm, LoginForm, TodoForm, EditForm
from flask_login import login_user, logout_user, current_user

todo = Blueprint('tasks', __name__)


@todo.route('/')
def home():
    return render_template('home.html')


@todo.route('/register', methods=['POST', 'GET'])
def register():
    form = RegisterForm()
    if request.method == 'POST':
        if form.validate_on_submit:
            user = User(first_name=form.first_name.data,
                        last_name=form.last_name.data,
                        email=form.email.data,
                        password=generate_password_hash(form.password.data)
                        )
            db.session.add(user)
            db.session.commit()
            return redirect('/login')

    else:
        return render_template('register.html', form=form)


@todo.route('/login', methods=['POST', 'GET'])
def login():
    form = LoginForm()
    if form.validate_on_submit:
        user = User.query.filter_by(email=form.email.data).first()
        if user and check_password_hash(user.password, form.password.data):
            login_user(user)
            return redirect('/todos')

    return render_template('login.html', form=form)


@todo.route('/logout', methods=['POST', 'GET'])
def logout():
    logout_user()
    return redirect('/')


@todo.route('/add_todo', methods=['POST', 'GET'])
def add_todo():
    form = TodoForm()
    method = request.method
    if method == 'GET':
        return render_template('add_todo.html', form=form)
    if method == 'POST' and form.validate_on_submit:
        todo = Task(task_name=form.task_name.data, status=form.status.data,
                    due_date=form.due_date.data, todo_owner=current_user.id
                    )
        db.session.add(todo)
        db.session.commit()
        return redirect('/todos')


@todo.route('/todos')
def todos():
    todos = Task.query.filter_by(todo_owner=current_user.id)
    return render_template('todos.html', todos=todos)


@todo.route('/edit_task/<int:id>', methods=['GET', 'POST'])
def edit_task(id):
    form = EditForm()
    task = Task.query.filter_by(id=id, todo_owner=current_user.id).first()
    method = request.method
    if method == 'POST' and form.validate_on_submit():
        task.task_name = form.task_name.data
        task.due_date = form.due_date.data
        task.status = form.status.data
        db.session.add(task)
        db.session.commit()
    if method == 'GET':
        form.task_name.data = task.task_name
        form.due_date.data = task.due_date
        form.status.data = task.status
        return render_template('edit_todo.html', form=form)
    return redirect('/todos')


@todo.route('/delete_task/<int:id>', methods=['GET', 'POST'])
def delete_task(id):
    task = Task.query.filter_by(id=id, todo_owner=current_user.id).first()
    if request.method == 'GET' and task:
        db.session.delete(task)
        db.session.commit()
        return redirect('/todos')
    abort(404)



